# Alphaworm: Chat

Kontin rakentaminen ja käynnistäminen (Jaakko Vanhalan ohjeesta kopioituna):

```shell=
npm install
docker build -t matopeli/chat .
docker run --net peliverkko --ip 172.18.0.4 -p 3004:3004 -it --name chat matopeli/chat
```

Käyttöliittymä on Alphawormin päähakemistossa nimellä `chatclient.html`.

Chat-viestit näkyvät myös selaimessa osoitteessa **172.18.0.4:3004**.

## Muutokset käynnissä olevan kontin sisältöön

Silloin, kun vain yksittäisiä tiedostoja täytyy päivittää, alla oleva menetelmä toimii. Jos konttiin tehdään isompi remontti, jossa kokonaisia paketteja pitää lisätä, on parempi rakentaa kontti uudelleen.

Esim. `chatserver.js`:n uuden version kopioiminen käynnissä olevaan chat-konttiin:

```shell=
docker cp chatserver.js chat:app/
docker restart chat
```

- Uudelleenkäynnistys `docker restart` -komennolla kestää pienen hetken.
- `chat:app/` tarkoittaa kontin sisäistä app-hakemistoa, joka on Dockerfilessa määritetty.
- Ennen `docker restart`-komennon ajamista voi tarkistaa, että konttiin kopioitu tiedosto näyttää siltä, miltä pitääkin:
  ```
  docker exec chat cat ./chatserver.js
  ```
- Jos jokin terminaali-ikkuna on yhdistetty konttiin (kuten että kontti on käynnistetty sieltä ajamalla `docker run [...]`), tämä yhteys katkeaa `docker restart`:n myötä. Uudelleen yhdistäminen on helppoa:
  ```
  docker attach chat
  ```
