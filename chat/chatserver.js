var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(process.env.APP_PORT || 8080);

/* Keskustelua voi seurata menemällä selaimella
     palvelimen osoitteeseen ja porttiin. */

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/monitor.html');
});

/* Viestityypit */

const messageTypes = [
  { id: 'msgChat', type: 'CHAT_MESSAGE' },
  { id: 'msgLoginReq', type: 'LOGIN_REQUEST' },
  { id: 'msgLogin', type: 'LOGIN' },
  { id: 'msgLoginFail', type: 'LOGIN_FAILED' },
  { id: 'msgLogout', type: 'LOGOUT' }
];

/* Tämä palvelisi vain clientia, joten se on poistettu tarpeettomana. */
/*
app.use('/messagetypes.js', function(req, res) {
  res.set('Content-Type', 'text/javascript');
  var messageTypeList = '';
  messageTypes.forEach((msg) => {
    messageTypeList += "var " + msg.id + " = '" + msg.type + "';\r\n";
  });
  res.send(messageTypeList);
});
*/

/* Socket.IO-toiminnot */

io.on('connection', function(socket) {

  console.log('Connected:', socket.id);

  socket.on('disconnect', function(message) {
    console.log('Disconnected:', socket.id);
    io.emit('LOGOUT', { from: socket.clientUserName || 'Someone' });
  });

  messageTypes.forEach((msg) => {
    socket.on(msg.type, function(message) {
      console.log(msg.type + ':', message);
      io.emit(msg.type, message);
    });
  });

  /* Ylimääräinen LOGIN-viestin käsittelijä liittää socketiin käyttäjänimen. */

  socket.on('LOGIN', function(message) {
    socket.clientUserName = message.from;
  });
});
