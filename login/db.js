var Database = function() {
    var database = this;
    // Luodaan tietokantayhteys.
    this.init = function() {
        if(undefined === this.connection) {
            console.log("init database connection");
            var mysql      = require('mysql');
            this.connection = mysql.createConnection({
                host     : '172.18.0.2',
                user     : 'root',
                password : '<salasana>',
                database : 'alphaworm'
             }); 
            this.connection.connect();
        };
    };

    function disconnect() {
        this.connection.end();
    };


    // Listataan käyttäjän socket kirjautuneeksi, mikäli tiedot ovat oikeat.
    this.login = function(userData,socket,clients){
        var parsedData = JSON.parse(userData);
        var logUsername = parsedData["Lusername"];
        var logPassword = parsedData["Lpassword"];

        var userQuery = "SELECT * FROM userdata WHERE username = (?)";
        this.connection.query( userQuery,[ logUsername ], function(err, rows, fields){
            try{

                // Tarkistellaan onko annettu salasana oikea käyttäjälle.
                if ( rows[0].password == logPassword){
                    console.log("Onnistunut login "+rows[0].username);
                    socket.username = rows[0].username;
                    console.log("Socket: "+socket.username);
                    for(var i = 0; i<clients.length;i++){
                    	if(socket.id == clients[i].id){
                    		clients[i].username = socket.username;
                    	}
                    }
                }
                else{
                    console.log("ei tunnistetta tietokannassa.");
                }
            }
            catch(err){
                console.log("ei tietokantavertailua.");
            }
        }); 
    };

    // Uuden käyttäjän rekisteröinti
    this.register = function(userData,socket){
        var parsedData = JSON.parse(userData);
        var username = parsedData["username"];
        var password = parsedData["password"];

        var checkQuery = "SELECT username FROM users WHERE username = (?)";
        
        if (username == "" || password == ""){
            socket.emit('Alert',"Pakolliset kentät ovat tyhjiä!");
        }

        else{
            // Onko käyttäjätunnus jo rekisteröity?
            this.connection.query(checkQuery, [username], function(err, result){
                if (err) throw err;

                // Rekisteröidään käyttäjä.
                if(result.length == 0){
                    database.addData(userData);    
                }
                else{
                    // Rekisteröidystä sähköpostista ilmoitus.
                    console.log("käyttäjänimi löytyy jo" + result);
                    socket.emit('Alert', "Käyttäjänimi on jo olemassa!");
                }
            });       
        }
    };
    // Apufunktio käyttäjän lisäämiseksi tietokantaan.
    this.addData = function(userData,socket){

        var parsedData = JSON.parse(userData);
        var username = parsedData["username"];
        var password = parsedData["password"];

        var addData = "INSERT INTO userdata (username, password_hash) VALUES (?, ?)";
        this.connection.query(addData, [ username, password ]);
    }




};

module.exports = Database;