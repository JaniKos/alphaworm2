// käyttäjän socket yhteys
var socket = io.connect();

// var socket = require('socket.io-client')('http://172.18.0.x:3004');
// Tähän tiedostoon nyt varmaan tarviis sen tiedon että mistä dockerista tieto
// Tieto haetaan ja se onnistus sit vaa tolla requiren avulla, yhistämällä oikeeseen dockeriin
// ei vaa jotenki avautunut tää iha täysi, mutta omassa localhost palvelussa sain toimimaan kyllä normaalisti

// Register ja login
        
        // Tallenetaan käyttäjän tiedot tähän JSON tauluun
        var user = {};
           
        // luetaan sivulta rekisteröintiin käytettävät tiedot ja siirretään tauluun user.
        function fetchRegister(){
          user.username = document.getElementById('username').value;
          user.password = document.getElementById('password').value;
        };

        // luetaan sivulta kirjautumiseen käytettävät tiedot ja siirretään tauluun user.
        function  fetchLogin(){
          user.Lusername = document.getElementById('username').value;
          user.Lpassword = document.getElementById('password').value;
        };

        // kirjaudutaan sisään, emitoidaan saatu data palvelimelle.
        function login() {
          fetchLogin();
          var data = JSON.stringify(user);
          console.log(data);
          socket.emit("loginUser",data);
        };

        // kirjaudutaan ulos.
        function logout() {
          socket.emit('logout')
        };

        // rekisteröidään käyttäjä, emitoidaan saatu data palvelimelle.
        function registerUser() {    
          console.log("Rekisteröinti");
          fetchRegister();
          var data = JSON.stringify(user);
          socket.emit("registerUser",data);           
        };
