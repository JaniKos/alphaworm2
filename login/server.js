
// tietokannan käyttöönotto
var database = require('./db');
var db = new database();
db.init();

// rekisteröinnin käyttöönotto
var register = require('./register');
var rg = new register(db);

// http://expressjs.com
var express = require('express');
var app = require('express')();
var server = require('http').Server(app);

// socket.io käyttöönotto
var io = require('socket.io')(server);
var http_clients = [];


// Mistä haetaan tiedostot, ei välttämätön??
app.use(express.static(__dirname + '/html'));

// socket.io yhteyden avaus

io.on('connection', function(socket) {
	// Lisätään käyttäjä aktiivisiin käyttäjiin.
	var user = {
		id: socket.id,
		username: undefined,
	};
	http_clients.push(user);

	// Poistetaan käyttäjä aktiivisista käyttäjistä kun yhteys suljetaan.
	socket.on('disconnect', function (){
		for(var i=0;i<http_clients.length;i++){

			if (http_clients[i].id === socket.id){
				http_clients.splice(i,1);
			}
		}
	});

	//Uuden käyttäjän rekisteröinti
	socket.on("registerUser", function(data){
		console.log("Käyttäjän rekisteröinti");
		rg.registerUser(data,socket);
	});

	//Sisäänkirjautuminen.
	socket.on("loginUser", function(data){
		var checkDouble = false;

		for (var i=0;i<http_clients.length;i++){
			if(http_clients[i].username == JSON.parse(data)["Lusername"]){
				checkDouble = true;
			}
		}
		if (socket.userID != undefined || checkDouble){
			console.log("olet jo kirjautunut");
			socket.emit('Alert',"Olet jo kirjautunut sisään!");
		}
		else{
			rg.loginUser(data,socket,http_clients);
		}

	});
});

// otetaan yhteys tietokantaan ja virheet ylös
/*connection.connect(function (err) {
	if (err) console.log(err);
});*/

/*
var server = app.listen(8000, function() {
  console.log('Server listening on port ' + server.address().port);
});*/
var host = "localhost";
var port = 8000;
server.listen(port,host, function () {
	console.log('Listening at '+host+":"+ port);
});

