//Server stuff
const express = require('express');
const bp = require('body-parser');
var app = express();
app.use(bp.json()); // support json encoded bodies
app.use(bp.urlencoded({ extended: true }));
var port = 8080;

var onlineUsers = [];


//REST API CALLS
//JSON object to attribute and putting to list
app.post('/post',function(req, res){
	var user = req.body.user;
	onlineUsers.push(user);

	res.send(user);
});

//JSON object to attribute and deleting from list
app.delete('/delete', function(req, res){
	var user = req.body.user;
	var i = 0;
	for (i; i < onlineUsers.length; i++) {

		if(user == onlineUsers[i]) {
			onlineUsers.splice(onlineUsers[i], 1);
		}
	}
});


app.listen(port);
console.log('listening port 8080');