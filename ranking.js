"use strict";

class Database {
	constructor() {
    console.log("Database object created");

    var mysql      = require('mysql');
    this.connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'project66',
      database : 'alphaworm'
    });

    this.connection.connect();
  }
}

class Ranking extends Database {

  getranking(callback) {

    this.connection.query("SELECT ??,?? FROM userdata ORDER BY highscore DESC", ["username", "highscore"],
     function(err, rows, fields) {
      if (err) {
      	throw err;
		}

      	console.log(rows);
      	callback(rows);
    });
  }
}

//var test = new Ranking();

//test.get();

module.exports = Ranking;