DROP DATABASE IF EXISTS alphaworm;
CREATE DATABASE alphaworm;
USE alphaworm;

CREATE TABLE users (
  username VARCHAR(12) PRIMARY KEY NOT NULL,
  highscore INT DEFAULT 0
);

insert into users (username, highscore) VALUES ('lasse', 200);
insert into users (username, highscore) VALUES ('lasse2000', 1000);