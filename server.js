<<<<<<< HEAD
#!/bin/env node

var MessageBroker = require('./server/messagebroker');
var MessageHandler = require('./server/messagehandler');
var GameAPI = require('./server/gameapi');

function GameServer() {

  // Set some internal variables
  this.setupVariables();

  // Call tnitialize and start server, when the server object is created
  this.init();
}

GameServer.prototype.setupVariables = function() {
  //  Environment variables for the OpenShift app
  this.ipaddress  = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
  this.port       = process.env.OPENSHIFT_NODEJS_PORT || 8080;
  // Applications domain name and port client shall connect
  this.domain     = process.env.OPENSHIFT_APP_DNS || '127.0.0.1';
  // Use websocket port 8000 in OpenShift, otherwise port 8080
  this.clientport = (process.env.OPENSHIFT_NODEJS_IP) ? 8000:8080;
}

GameServer.prototype.init = function() {
  console.log("Initialising server...");
  var http = require('http'),
  express = require('express');
  this.app = express();

  // Pass the websocket information to the client
  this.app.use('/websocketURI.js', function(req, res) {
    console.log("domain = ", gameServer.domain, "port:", gameServer.clientport);

    var websocketURI = gameServer.domain + ':' + gameServer.clientport;
    console.log("websocketURI = ", websocketURI);
    res.set('Content-Type', 'text/javascript');
    res.send('var websocketURI="' + websocketURI + '";');
  });

  // Enable some directories for browsers
  this.app.use('/client', express.static(__dirname + '/client'));
  this.app.use('/common', express.static(__dirname + '/common'));
  this.app.use('/media', express.static(__dirname + '/media'));

  // Return index.html to browsers
  this.app.get('/', function(req, res) {
    console.log("loading index.html");
    res.sendfile('index.html');
  });

  // Create server application (express.js)
  this.serverapp = http.createServer(this.app);
  this.serverapp.listen(this.port, this.ipaddress);
  console.log("game server running @ http://" + this.ipaddress + ":" + this.port);

  // Create server objects
  this.messageBroker = new MessageBroker(this);
  this.messageHandler = new MessageHandler();
  this.gameAPI = new GameAPI();
  // Link the objects:
  // Incoming: MessageBroker -> MessageHandler -> GameAPI
  this.messageBroker.attachMessageHandler(this.messageHandler);
  this.messageHandler.attachGameAPI(this.gameAPI);
  // Outgoing: GameAPI -> MessageHandler -> MessageBroker
  this.gameAPI.attachMessageHandler(this.messageHandler);
  this.messageHandler.attachMessageBroker(this.messageBroker);

  console.log("game server started");
}

// Create the game server
var gameServer = new GameServer();
=======
var Ranking = require('./ranking');


var express = require('express');
var bodyParser = require('body-parser')

var app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));


var ranking = new Ranking();

var mysql      = require('mysql');
    connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'project66',
      database : 'alphaworm'
    });

// application -------------------------------------------------------------
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/client/index.html'); // load the index file
});

app.get('/users/ranking', function (req, res) {
  var paluuarvo = ranking.getranking(function(rows) {   
    res.send(rows);
  });
  
});


//Get all users ei tarvita

app.get("/users",function(req,res){

        var query = "SELECT * FROM userdata";
        //var table = ["userdata","username","highscore",req.body.username,md5(req.body.highscore)];
        //query = mysql.format(query);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                console.log(rows);
                res.send(rows);
                //res.json({"Error" : false, "Message" : "Users found !"});
            }
        });
    });

// Get by username, tarkistetaan, onko jo olemassa...

app.get("/users/:user_id",function(req,res){
        var query = "SELECT * FROM userdata WHERE ??=?";
        var table = ["username","user_id",req.params.username];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query"});
            } else {
                res.json({"Error" : false, "Message" : "Success", "Users" : rows});
                
            }
        });
    });


//Tähän toinen samanlainen get-pyyntö, password mukaan
//get username + password(haetaan ja tarkistetaan username ja password
//req.params.username, req.params.password
//Jos selaimelle syötetyt samat kun tietokannassa ------> Online-feature?



//Lisää käyttäjä tietokantaan. Pitää tehdä ensin ylempi Get by username-tarkistus ja vertailu
// ennen tätä, ettei oo samannimistä...ehkä jopa passwordvertailu "olet jo rekisteröinyt ittes"
//tai käyttää mysql-määrityksiä jotenki hyväksi, unique jne --> jos virhe, niin --->
app.post("/users",function(req,res){
        var query = "INSERT INTO userdata (username, password) VALUES (?,?)";
        var table = [req.body.username, req.body.password];
        
        query = mysql.format(query,table);
        console.log(query);
       connection.query(query,function(err,rows){
           if(err) {
                res.json({"Error" : true, "Message" : "Error executing MySQL query", err});
            } else {
                res.json({"Error" : false, "Message" : "User Added !"});
            }
       });
    });


  



app.listen(3000, function () {
  console.log('Example app listening http://localhost:3000');
});
















// #!/bin/env node

// var MessageBroker = require('./server/messagebroker');
// var MessageHandler = require('./server/messagehandler');
// var GameAPI = require('./server/gameapi');

// function GameServer() {

//   // Set some internal variables
//   this.setupVariables();

//   // Call tnitialize and start server, when the server object is created
//   this.init();
// }

// GameServer.prototype.setupVariables = function() {
//   //  Environment variables for the OpenShift app
//   this.ipaddress  = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
//   this.port       = process.env.OPENSHIFT_NODEJS_PORT || 8080;
//   // Applications domain name and port client shall connect
//   this.domain     = process.env.OPENSHIFT_APP_DNS || '127.0.0.1';
//   // Use websocket port 8000 in OpenShift, otherwise port 8080
//   this.clientport = (process.env.OPENSHIFT_NODEJS_IP) ? 8000:8080;
// }

// GameServer.prototype.init = function() {
//   console.log("Initialising server...");
//   var http = require('http'),
//   express = require('express');
//   this.app = express();

//   // Pass the websocket information to the client
//   this.app.use('/websocketURI.js', function(req, res) {
//     console.log("domain = ", gameServer.domain, "port:", gameServer.clientport);

//     var websocketURI = gameServer.domain + ':' + gameServer.clientport;
//     console.log("websocketURI = ", websocketURI);
//     res.set('Content-Type', 'text/javascript');
//     res.send('var websocketURI="' + websocketURI + '";');
//   });

//   // Enable some directories for browsers
//   this.app.use('/client', express.static(__dirname + '/client'));
//   this.app.use('/common', express.static(__dirname + '/common'));
//   this.app.use('/media', express.static(__dirname + '/media'));

//   // Return index.html to browsers
//   this.app.get('/', function(req, res) {
//     console.log("loading index.html");
//     res.sendfile('index.html');
//   });

//   // Create server application (express.js)
//   this.serverapp = http.createServer(this.app);
//   this.serverapp.listen(this.port, this.ipaddress);
//   console.log("game server running @ http://" + this.ipaddress + ":" + this.port);

//   // Create server objects
//   this.messageBroker = new MessageBroker(this);
//   this.messageHandler = new MessageHandler();
//   this.gameAPI = new GameAPI();
//   // Link the objects:
//   // Incoming: MessageBroker -> MessageHandler -> GameAPI
//   this.messageBroker.attachMessageHandler(this.messageHandler);
//   this.messageHandler.attachGameAPI(this.gameAPI);
//   // Outgoing: GameAPI -> MessageHandler -> MessageBroker
//   this.gameAPI.attachMessageHandler(this.messageHandler);
//   this.messageHandler.attachMessageBroker(this.messageBroker);

//   console.log("game server started");
// }

// // Create the game server
// var gameServer = new GameServer();
>>>>>>> origin/login
